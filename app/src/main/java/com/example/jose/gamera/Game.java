package com.example.jose.gamera;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Jose on 01/05/2018.
 */

public class Game implements Serializable {

    int idGame;
    String nameGame;
    Date startDateGame;
    Date finishDateGame;
    String developerGame;
    Date releaseDateGame;
    String genreGame;
    int numberOfTimesCompletedGame;
    String platformGame;

    private int incrementalIdGame = 0;

    public Game() {
    }

    //Builder for wishlist
    public Game(String nameGame, String developerGame, Date releaseDateGame, String genreGame, String platformGame) {
        this.idGame = incrementalIdGame++;
        this.nameGame = nameGame;
        this.developerGame = developerGame;
        this.releaseDateGame = releaseDateGame;
        this.genreGame = genreGame;
        this.platformGame = platformGame;
    }

    //Builder for games currently playing
    public Game(int idGame, String nameGame, Date startDateGame, String developerGame, Date releaseDateGame, String genreGame, String platformGame) {
        this.idGame = idGame;
        this.nameGame = nameGame;
        this.startDateGame = startDateGame;
        this.developerGame = developerGame;
        this.releaseDateGame = releaseDateGame;
        this.genreGame = genreGame;
        this.platformGame = platformGame;
    }

    //Builder for games played
    public Game(int idGame, String nameGame, Date startDateGame, Date finishDateGame, String developerGame, Date releaseDateGame, String genreGame, int numberOfTimesCompletedGame, String platformGame) {
        this.idGame = idGame;
        this.nameGame = nameGame;
        this.startDateGame = startDateGame;
        this.finishDateGame = finishDateGame;
        this.developerGame = developerGame;
        this.releaseDateGame = releaseDateGame;
        this.genreGame = genreGame;
        this.numberOfTimesCompletedGame = numberOfTimesCompletedGame;
        this.platformGame = platformGame;
    }

    public String getNameGame() {
        return nameGame;
    }

}
