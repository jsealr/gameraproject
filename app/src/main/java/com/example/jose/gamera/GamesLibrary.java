package com.example.jose.gamera;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class GamesLibrary implements Serializable {
    ArrayList<Game> arrayListGames = new ArrayList<Game>();

    public GamesLibrary() {

        arrayListGames.add(new Game("Mario", "Nintendo", new Date(2018, 7, 12), "Platform", "Nintendo Switch" ));
        arrayListGames.add(new Game("Metroid", "Sony", new Date(2018, 5, 13), "Action", "PlayStation 4" ));
        arrayListGames.add(new Game("Fox", "Montreal", new Date(2018, 4, 23), "RPG", "Xbox" ));
        arrayListGames.add(new Game("Prince of Persia", "Camposanto", new Date(2018, 6, 27), "JRPG", "PC" ));
        arrayListGames.add(new Game("sadsad", "dasdasd", new Date(2018, 8, 21), "sdfsdf", "sdfsdfsdf" ));

    }

    public void setGame(Game game) {
        arrayListGames.add(game);
    }

    public Game getGame(int idGame) {
        for(Game game : arrayListGames) {
            if(game.idGame == idGame) return game;
        }

        for(int i = 0; i < arrayListGames.size(); i++) {
            if(arrayListGames.get(i).idGame == idGame) return arrayListGames.get(i);
        }
        return null;
    }

    public String[] getGameNames(){
        int num= arrayListGames.size();

        String[] nameGames = new String[num];

        for (int i=0; i<arrayListGames.size();i++) {
            nameGames[i]=arrayListGames.get(i).getNameGame();
        }
        return nameGames;
    }

    public ArrayList<Game> getAllGames() {

        return arrayListGames;
    }
}

