package com.example.jose.gamera;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.jose.gamera.View.GameView;

import java.io.Serializable;


public class ListViewGames {

    private Context context;
    private ListView captureListView;
    GamesLibrary gL = new GamesLibrary();


    public ListViewGames(Context context, ListView listViewGames) {

        captureListView = listViewGames;
        this.context = context;
    }

    public void showListView() {

        //Adaptador: Permiten llenar elementos
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.support_simple_spinner_dropdown_item, gL.getGameNames());

        captureListView.setAdapter(adapter);
        captureListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent i = new Intent(context, GameView.class);
                i.putExtra("Game",  gL.arrayListGames.get(position));

                context.startActivity(i);
            }
        });
    }
}
