package com.example.jose.gamera.View;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.example.jose.gamera.Game;
import com.example.jose.gamera.R;

public class GameView extends Activity{

    Game gameBundle;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sampleGameView();
        textView = findViewById(R.id.textViewGameSample);


        Bundle extras = getIntent().getExtras();
        gameBundle = (Game) extras.get("Game");

        textView.setText(gameBundle.getNameGame().toString());

    }

    public void sampleGameView(){



       setContentView(R.layout.game_view);
   }


}
